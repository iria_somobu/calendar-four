package org.somobu.todoagenda.prefs;

import static android.content.Intent.ACTION_CREATE_DOCUMENT;
import static com.somobu.calendar.preferences.WidgetConfigurationActivity.REQUEST_ID_BACKUP_SETTINGS;
import static com.somobu.calendar.preferences.WidgetConfigurationActivity.REQUEST_ID_RESTORE_SETTINGS;
import static org.somobu.todoagenda.util.DateUtil.formatLogDateTime;

import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.support.annotation.Nullable;

import com.somobu.calendar.R;

public class WidgetBackupPreferencesFragment extends PreferenceFragment {

    private static final String KEY_BACKUP_SETTINGS = "backupSettings";
    private static final String KEY_RESTORE_SETTINGS = "restoreSettings";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.widget_pref_backup);
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen screen, Preference preference) {
        int widgetId = ApplicationPreferences.getWidgetId(getActivity());
        ApplicationPreferences.save(getActivity(), widgetId);
        switch (preference.getKey()) {
            case KEY_BACKUP_SETTINGS:
                InstanceSettings settings = AllSettings.instanceFromId(getActivity(), widgetId);
                String fileName = (settings.getWidgetInstanceName() + "-" + getText(R.string.app_name))
                        .replaceAll("\\W+", "-") +
                        "-backup-" + formatLogDateTime(System.currentTimeMillis()) +
                        ".json";
                Intent intent = new Intent(ACTION_CREATE_DOCUMENT);
                intent.setType("application/json");
                intent.putExtra(Intent.EXTRA_TITLE, fileName);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                getActivity().startActivityForResult(intent, REQUEST_ID_BACKUP_SETTINGS);
                break;
            case KEY_RESTORE_SETTINGS:
                Intent intent2 = new Intent()
                        .setType("*/*")
                        .setAction(Intent.ACTION_GET_CONTENT)
                        .addCategory(Intent.CATEGORY_OPENABLE);
                Intent withChooser = Intent.createChooser(intent2,
                        getActivity().getText(R.string.restore_settings_title));
                getActivity().startActivityForResult(withChooser, REQUEST_ID_RESTORE_SETTINGS);
                break;
            default:
                break;
        }
        return super.onPreferenceTreeClick(screen, preference);
    }
}