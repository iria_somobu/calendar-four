package org.somobu.todoagenda.prefs;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;


import com.somobu.calendar.R;
import org.somobu.todoagenda.prefs.colors.ColorThemeType;


public class RootFragment extends PreferenceFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.widget_pref_root);
        setTitles();
    }

    @Override
    public void onResume() {
        super.onResume();
        Activity activity = getActivity();
        if(activity != null) {
            activity.setTitle(ApplicationPreferences.getWidgetInstanceName(activity));
        }
        setTitles();
    }

    private void setTitles() {
        Context context = getActivity();
        Preference preference = findPreference("ColorsPreferencesFragment");
        if (context != null && preference != null) {
            ColorThemeType themeType = ApplicationPreferences.getEditingColorThemeType(context);
            preference.setTitle(themeType.titleResId);
            preference.setEnabled(themeType.isValid());
        }
    }
}