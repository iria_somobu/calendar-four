package org.somobu.todoagenda.prefs;

import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;


import com.somobu.calendar.R;

public class EventDetailsPreferencesFragment extends PreferenceFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.widget_pref_event_details);
    }

}