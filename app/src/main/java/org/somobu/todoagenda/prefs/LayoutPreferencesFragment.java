package org.somobu.todoagenda.prefs;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;


import com.somobu.calendar.R;

public class LayoutPreferencesFragment extends PreferenceFragment
        implements SharedPreferences.OnSharedPreferenceChangeListener {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.widget_pref_layout);
    }

    @Override
    public void onResume() {
        super.onResume();
        showEventEntryLayout();
        showWidgetHeaderLayout();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    private void showEventEntryLayout() {
        Preference preference = findPreference(InstanceSettings.PREF_EVENT_ENTRY_LAYOUT);
        if (preference != null) {
            preference.setSummary(ApplicationPreferences.getEventEntryLayout(getActivity()).summaryResId);
        }
    }

    private void showWidgetHeaderLayout() {
        Preference preference = findPreference(InstanceSettings.PREF_WIDGET_HEADER_LAYOUT);
        if (preference != null) {
            preference.setSummary(ApplicationPreferences.getWidgetHeaderLayout(getActivity()).summaryResId);
        }
    }

/*    @Override
    public void onDisplayPreferenceDialog(Preference preference) {
        DialogFragment dialogFragment = null;
        if (preference instanceof DateFormatPreference) {
            dialogFragment = new DateFormatDialog((DateFormatPreference) preference);
        }

        if (dialogFragment != null) {
            dialogFragment.setTargetFragment(this, 0);
            dialogFragment.show(getFragmentManager(), FRAGMENT_TAG);
        } else {
            super.onDisplayPreferenceDialog(preference);
        }
    } */

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        switch (key) {
            case InstanceSettings.PREF_EVENT_ENTRY_LAYOUT:
                showEventEntryLayout();
                break;
            case InstanceSettings.PREF_WIDGET_HEADER_LAYOUT:
                showWidgetHeaderLayout();
                break;
            default:
                break;
        }
    }
}