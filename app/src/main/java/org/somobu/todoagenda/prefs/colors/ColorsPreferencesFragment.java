package org.somobu.todoagenda.prefs.colors;

import static com.somobu.calendar.preferences.WidgetConfigurationActivity.EXTRA_GOTO_SECTION_COLORS;
import static com.somobu.calendar.preferences.WidgetConfigurationActivity.FRAGMENT_TAG;
import static org.somobu.todoagenda.prefs.ApplicationPreferences.PREF_DIFFERENT_COLORS_FOR_DARK;
import static org.somobu.todoagenda.prefs.colors.ThemeColors.PREF_TEXT_COLOR_SOURCE;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import android.support.annotation.Nullable;

import com.somobu.calendar.R;
import com.somobu.calendar.preferences.WidgetConfigurationActivity;
import com.somobu.calendar.prefviews.holopicker.HoloColorPickerPreference;

import org.somobu.todoagenda.prefs.ApplicationPreferences;
import org.somobu.todoagenda.prefs.InstanceSettings;
import org.somobu.todoagenda.prefs.MyPreferenceFragment;
import org.somobu.todoagenda.widget.TimeSection;

import java.util.ArrayList;
import java.util.List;

/**
 * AndroidX version created by yvolk@yurivolkov.com
 * based on this answer: https://stackoverflow.com/a/53290775/297710
 * and on the code of https://github.com/koji-1009/ChronoDialogPreference
 */
public class ColorsPreferencesFragment extends MyPreferenceFragment
        implements SharedPreferences.OnSharedPreferenceChangeListener {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.widget_pref_colors);
        removeUnavailablePreferences();
    }

    @Override
    public void onResume() {
        super.onResume();

        removeUnavailablePreferences();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        showTextSources();
    }

    private void showTextSources() {
        Context context = getActivity();
        if (context != null) {
            TextColorSource textColorSource = ApplicationPreferences.getTextColorSource(context);
            Preference preference = findPreference(PREF_TEXT_COLOR_SOURCE);
            if (preference != null) {
                preference.setSummary(context.getString(textColorSource.titleResId) + "\n" +
                        context.getString(textColorSource.summaryResId));
            }
            if (textColorSource == TextColorSource.SHADING) {
                showShadings();
            }
            previewTextOnBackground();
        }
    }

    private void previewTextOnBackground() {
        ThemeColors colors = getSettings().colors();
        for (BackgroundColorPref backgroundColorPref : BackgroundColorPref.values()) {
            HoloColorPickerPreference colorPreference = (HoloColorPickerPreference) findPreference(backgroundColorPref.colorPreferenceName);
            if (colorPreference != null) {
                List<TextColorPref> toPreview = new ArrayList<>();

                for (TextColorPref pref : TextColorPref.values()) {
                    if (pref.backgroundColorPref == backgroundColorPref) {
                        toPreview.add(pref);
                    }
                }

/*                if (toPreview.size() > 0) {
                    TextColorPref pref = toPreview.get(0);
                    colorPreference.setSampleTextColor1(colors.getTextColor(pref, pref.colorAttrId));
                }

                if (toPreview.size() > 1) {
                    TextColorPref pref = toPreview.get(1);
                    colorPreference.setSampleTextColor2(colors.getTextColor(pref, pref.colorAttrId));
                } */
            }
        }
    }

    private void removeUnavailablePreferences() {
        Context context = getActivity();
        if (context == null) return;

        ColorThemeType colorThemeType = ApplicationPreferences.getColorThemeType(context);
        if (!ColorThemeType.canHaveDifferentColorsForDark() ||
                colorThemeType == ColorThemeType.LIGHT ||
                colorThemeType == ColorThemeType.SINGLE && !InstanceSettings.isDarkThemeOn(context)) {
            PreferenceScreen screen = getPreferenceScreen();
            Preference preference = findPreference(PREF_DIFFERENT_COLORS_FOR_DARK);
            if (screen != null && preference != null) {
                screen.removePreference(preference);
            }
        }
        if (ApplicationPreferences.noPastEvents(context)) {
            PreferenceScreen screen = getPreferenceScreen();
            Preference preference = findPreference(TimeSection.PAST.preferenceCategoryKey);
            if (screen != null && preference != null) {
                screen.removePreference(preference);
            }
        }
        switch (ApplicationPreferences.getTextColorSource(context)) {
            case AUTO:
                removeShadings();
                removeTextColors();
                break;
            case SHADING:
                removeTextColors();
                break;
            case COLORS:
                removeShadings();
                break;
        }
    }

    private void removeShadings() {
        for (TextColorPref pref : TextColorPref.values()) {
            removePreferenceImproved(pref.shadingPreferenceName);
        }
    }

    private void removePreferenceImproved(String preferenceName) {
        Preference preference = findPreference(preferenceName);
        PreferenceScreen screen = getPreferenceScreen();
        if (screen != null && preference != null) {
            screen.removePreference(preference);
        }
    }

    private void removeTextColors() {
        for (TextColorPref pref : TextColorPref.values()) {
            removePreferenceImproved(pref.colorPreferenceName);
        }
    }

    private void showShadings() {
        for (TextColorPref shadingPref : TextColorPref.values()) {
            ListPreference preference = (ListPreference) findPreference(shadingPref.shadingPreferenceName);
            if (preference != null) {
                Shading shading = Shading.fromThemeName(preference.getValue(), shadingPref.defaultShading);
                preference.setSummary(getActivity().getString(shading.titleResId));
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Activity activity = getActivity();
        switch (key) {
            case PREF_DIFFERENT_COLORS_FOR_DARK:
                if (activity != null) {
                    if (ApplicationPreferences.getEditingColorThemeType(activity) == ColorThemeType.NONE) {
                        activity.startActivity(WidgetConfigurationActivity.intentToStartMe(activity, ApplicationPreferences.getWidgetId(activity)));
                        activity.finish();
                        return;
                    }
                }
                break;
            case PREF_TEXT_COLOR_SOURCE:
                if (activity != null) {
                    Intent intent = WidgetConfigurationActivity.intentToStartMe(activity, ApplicationPreferences.getWidgetId(activity));
                    intent.putExtra(WidgetConfigurationActivity.EXTRA_GOTO_PREFERENCES_SECTION, EXTRA_GOTO_SECTION_COLORS);
                    activity.startActivity(intent);
                    activity.finish();
                    return;
                }
                break;
            default:
                saveSettings();
                showTextSources();
                break;
        }
    }
}