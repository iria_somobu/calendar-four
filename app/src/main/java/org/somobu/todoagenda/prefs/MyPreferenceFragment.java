package org.somobu.todoagenda.prefs;

import android.preference.PreferenceFragment;

public abstract class MyPreferenceFragment extends PreferenceFragment {

    public InstanceSettings getSettings() {
        return AllSettings.instanceFromId(getActivity(), getWidgetId());
    }

    public int getWidgetId() {
        return ApplicationPreferences.getWidgetId(getActivity());
    }

    public void saveSettings() {
        ApplicationPreferences.save(getActivity(), getWidgetId());
    }
}
