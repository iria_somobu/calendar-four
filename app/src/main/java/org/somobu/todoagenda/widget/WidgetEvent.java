package org.somobu.todoagenda.widget;

import org.somobu.todoagenda.prefs.OrderedEventSource;

public interface WidgetEvent {
    OrderedEventSource getEventSource();
    long getEventId();
}
