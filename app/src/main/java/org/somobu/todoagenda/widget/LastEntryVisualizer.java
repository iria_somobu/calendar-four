package org.somobu.todoagenda.widget;

import static org.somobu.todoagenda.RemoteViewsFactory.ACTION_CONFIGURE;
import static org.somobu.todoagenda.RemoteViewsFactory.getActionPendingIntent;
import static org.somobu.todoagenda.util.CalendarIntentUtil.newOpenCalendarAtDayIntent;
import static org.somobu.todoagenda.util.RemoteViewsUtil.setBackgroundColor;
import static org.somobu.todoagenda.util.RemoteViewsUtil.setTextColor;
import static org.somobu.todoagenda.util.RemoteViewsUtil.setTextSize;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.RemoteViews;

import com.somobu.calendar.R;
import com.somobu.calendar.preferences.WidgetConfigurationActivity;

import org.joda.time.DateTime;
import org.somobu.todoagenda.prefs.colors.TextColorPref;
import org.somobu.todoagenda.provider.EventProvider;
import org.somobu.todoagenda.provider.EventProviderType;

import java.util.Collections;
import java.util.List;

/**
 * @author yvolk@yurivolkov.com
 */
public class LastEntryVisualizer extends WidgetEntryVisualizer<LastEntry> {
    private static final String TAG = LastEntryVisualizer.class.getSimpleName();

    public LastEntryVisualizer(Context context, int widgetId) {
        super(new EventProvider(EventProviderType.LAST_ENTRY, context, widgetId));
    }

    @Override
    @NonNull
    public RemoteViews getRemoteViews(WidgetEntry eventEntry, int position) {
        LastEntry entry = (LastEntry) eventEntry;
        Log.d(TAG, "lastEntry: " + entry.type);
        RemoteViews rv = new RemoteViews(getContext().getPackageName(), entry.type.layoutId);

        int viewId = R.id.event_entry;
        if (position < 0) {
            rv.setOnClickPendingIntent(R.id.event_entry, getActionPendingIntent(getSettings(), ACTION_CONFIGURE));
        }
        if (entry.type == LastEntry.LastEntryType.EMPTY && getSettings().noPastEvents()) {
            rv.setTextViewText(viewId, getContext().getText(R.string.no_upcoming_events));
        }
        setTextSize(getSettings(), rv, viewId, R.dimen.event_entry_title);
        setTextColor(getSettings(), TextColorPref.forTitle(entry), rv, viewId, R.attr.eventEntryTitle);
        setBackgroundColor(rv, viewId, getSettings().colors().getEntryBackgroundColor(entry));
        return rv;
    }

    @Override
    public Intent newViewEntryIntent(WidgetEntry widgetEntry) {
        LastEntry entry = (LastEntry) widgetEntry;
        switch (entry.type) {
            case EMPTY:
            case NOT_LOADED:
                return newOpenCalendarAtDayIntent(new DateTime(getSettings().clock().getZone()));
            default:
                break;
        }
        return WidgetConfigurationActivity.intentToStartMe(getSettings().getContext(), getSettings().getWidgetId());
    }

    @Override
    public List<LastEntry> queryEventEntries() {
        return Collections.emptyList();
    }
}