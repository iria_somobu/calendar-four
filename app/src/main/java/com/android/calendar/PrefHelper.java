package com.android.calendar;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.somobu.calendar.R;

public class PrefHelper {

    // Preference keys
    public static final String KEY_HIDE_DECLINED = "preferences_hide_declined";
    public static final String KEY_WEEK_START_DAY = "preferences_week_start_day";
    public static final String KEY_SHOW_WEEK_NUM = "preferences_show_week_num";
    public static final String KEY_DAYS_PER_WEEK = "preferences_days_per_week";
    public static final String KEY_SKIP_SETUP = "preferences_skip_setup";
    public static final String KEY_CLEAR_SEARCH_HISTORY = "preferences_clear_search_history";
    public static final String KEY_ALERTS_CATEGORY = "preferences_alerts_category";
    public static final String KEY_ALERTS = "preferences_alerts";
    public static final String KEY_ALERTS_VIBRATE = "preferences_alerts_vibrate";
    public static final String KEY_ALERTS_RINGTONE = "preferences_alerts_ringtone";
    public static final String KEY_ALERTS_POPUP = "preferences_alerts_popup";
    public static final String KEY_SHOW_CONTROLS = "preferences_show_controls";
    public static final String KEY_DEFAULT_REMINDER = "preferences_default_reminder";
    public static final int NO_REMINDER = -1;
    public static final String NO_REMINDER_STRING = "-1";
    public static final int REMINDER_DEFAULT_TIME = 10; // in minutes
    public static final String KEY_USE_CUSTOM_SNOOZE_DELAY = "preferences_custom_snooze_delay";
    public static final String KEY_DEFAULT_SNOOZE_DELAY = "preferences_default_snooze_delay";
    public static final int SNOOZE_DELAY_DEFAULT_TIME = 5; // in minutes
    public static final String KEY_DEFAULT_CELL_HEIGHT = "preferences_default_cell_height";
    public static final String KEY_VERSION = "preferences_version";

    /**
     * Key to SharePreference for default view (CalendarController.ViewType)
     */
    public static final String KEY_START_VIEW = "preferred_startView";
    /**
     * Key to SharePreference for default detail view (CalendarController.ViewType)
     * Typically used by widget
     */
    public static final String KEY_DETAILED_VIEW = "preferred_detailedView";
    public static final String KEY_DEFAULT_CALENDAR = "preference_defaultCalendar";

    // These must be in sync with the array preferences_week_start_day_values
    public static final String WEEK_START_DEFAULT = "-1";
    public static final String WEEK_START_SATURDAY = "7";
    public static final String WEEK_START_SUNDAY = "1";
    public static final String WEEK_START_MONDAY = "2";

    // Default preference values
    public static final int DEFAULT_START_VIEW = CalendarController.ViewType.WEEK;
    public static final int DEFAULT_DETAILED_VIEW = CalendarController.ViewType.DAY;
    public static final boolean DEFAULT_SHOW_WEEK_NUM = false;

    // This should match the XML file.
    public static final String DEFAULT_RINGTONE = "content://settings/system/notification_sound";

    // The name of the shared preferences file. This name must be maintained for historical
    // reasons, as it's what PreferenceManager assigned the first time the file was created.
    public static final String SHARED_PREFS_NAME = "com.android.calendar_preferences";
    public static final String SHARED_PREFS_NAME_NO_BACKUP = "com.android.calendar_preferences_no_backup";
    // Must be the same keys that are used in the other_preferences.xml file.
    public static final String KEY_OTHER_QUIET_HOURS = "preferences_reminders_quiet_hours";
    public static final String KEY_OTHER_REMINDERS_RESPONDED = "preferences_reminders_responded";
    public static final String KEY_OTHER_QUIET_HOURS_START =
            "preferences_reminders_quiet_hours_start";
    public static final String KEY_OTHER_QUIET_HOURS_START_HOUR =
            "preferences_reminders_quiet_hours_start_hour";
    public static final String KEY_OTHER_QUIET_HOURS_START_MINUTE =
            "preferences_reminders_quiet_hours_start_minute";
    public static final String KEY_OTHER_QUIET_HOURS_END =
            "preferences_reminders_quiet_hours_end";
    public static final String KEY_OTHER_QUIET_HOURS_END_HOUR =
            "preferences_reminders_quiet_hours_end_hour";
    public static final String KEY_OTHER_QUIET_HOURS_END_MINUTE =
            "preferences_reminders_quiet_hours_end_minute";
    public static final String KEY_OTHER_1 = "preferences_tardis_1";
    public static final int QUIET_HOURS_DEFAULT_START_HOUR = 22;
    public static final int QUIET_HOURS_DEFAULT_START_MINUTE = 0;
    public static final int QUIET_HOURS_DEFAULT_END_HOUR = 8;
    public static final int QUIET_HOURS_DEFAULT_END_MINUTE = 0;

    /**
     * Return a properly configured SharedPreferences instance
     */
    public static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE);
    }

    /**
     * Set the default shared preferences in the proper context
     */
    public static void setDefaultValues(Context context) {
        PreferenceManager.setDefaultValues(context, SHARED_PREFS_NAME, Context.MODE_PRIVATE,
                R.xml.pref_general, false);
    }
}
