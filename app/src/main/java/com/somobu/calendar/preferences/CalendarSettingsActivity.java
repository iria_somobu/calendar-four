/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.somobu.calendar.preferences;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ActionBar;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceActivity;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Calendars;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.android.calendar.selectcalendars.SelectCalendarsSyncFragment;

import com.somobu.calendar.R;

import org.somobu.todoagenda.prefs.AllSettings;
import org.somobu.todoagenda.prefs.InstanceSettings;
import org.sufficientlysecure.ical.ui.MainActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalendarSettingsActivity extends PreferenceActivity {

    private static final int CHECK_ACCOUNTS_DELAY = 3000;
    private final Handler mHandler = new Handler();
    private List<Header> headers = null;
    private Account[] mAccounts = {};

    @Override
    public void onBuildHeaders(List<Header> target) {
        fillHeaders(target);
        this.headers = target;
    }

    private void fillHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.calendar_settings_headers, target);

        addCalIOEntry(target);
        fillWidgetsCategory(target);
        fillAccountsCategory(target);
    }

    private void addCalIOEntry(List<Header> target) {
        Header widgetHeader = new Header();
        widgetHeader.title = "Import/export";
        widgetHeader.intent = new Intent(this, MainActivity.class);
        target.add(target.size() - 2, widgetHeader);
    }
    
    private void fillWidgetsCategory(List<Header> target) {
        if (AllSettings.getInstances(this).isEmpty()) return;

        Header widgetsDivider = new Header();
        widgetsDivider.title = "Widgets";
        target.add(widgetsDivider);

        for (InstanceSettings settings : AllSettings.getInstances(this).values()) {
            String name = settings.getWidgetInstanceName();
            int id = settings.getWidgetId();

            Header widgetHeader = new Header();
            widgetHeader.title = name;
            widgetHeader.intent = WidgetConfigurationActivity.intentToStartMe(this, id);
            target.add(widgetHeader);
        }
    }

    private void fillAccountsCategory(List<Header> target) {
        Account[] accounts = AccountManager.get(this).getAccounts();
        mAccounts = accounts;

        Header accountsDivider = new Header();
        accountsDivider.title = "Accounts";
        target.add(accountsDivider);

        int accountsAdded = 0;
        for (Account acct : accounts) {
            if (ContentResolver.getIsSyncable(acct, CalendarContract.AUTHORITY) > 0) {
                Header accountHeader = new Header();
                accountHeader.title = acct.name;
                accountHeader.fragment = SelectCalendarsSyncFragment.class.getCanonicalName();

                Bundle args = new Bundle();
                args.putString(Calendars.ACCOUNT_NAME, acct.name);
                args.putString(Calendars.ACCOUNT_TYPE, acct.type);
                accountHeader.fragmentArguments = args;

                target.add(accountHeader);
                accountsAdded++;
            }
        }

        if (accountsAdded == 0) target.remove(accountsDivider);
    }

    @Override
    protected boolean isValidFragment(String fragmentName) {
        return true;
    }

    @Override
    public void setListAdapter(ListAdapter adapter) {
        if (adapter == null) {
            super.setListAdapter(null);
        } else {
            if (headers == null) {
                headers = new ArrayList<>();
                fillHeaders(headers);
            }
            super.setListAdapter(new HeaderAdapter(this, headers));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ActionBar ab = getActionBar();
        if (ab != null) {
            ab.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP, ActionBar.DISPLAY_HOME_AS_UP);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        mHandler.postDelayed(mCheckAccounts, CHECK_ACCOUNTS_DELAY);
        super.onResume();
    }

    @Override
    public void onPause() {
        mHandler.removeCallbacks(mCheckAccounts);
        super.onPause();
    }

    Runnable mCheckAccounts = new Runnable() {
        @Override
        public void run() {
            Account[] accounts = AccountManager.get(CalendarSettingsActivity.this).getAccounts();
            if (!Arrays.equals(accounts, mAccounts)) {
                invalidateHeaders();
            }
        }
    };


    private static class HeaderAdapter extends ArrayAdapter<Header> {
        static final int HEADER_TYPE_CATEGORY = 0;
        static final int HEADER_TYPE_NORMAL = 1;
        private static final int HEADER_TYPE_COUNT = 2;


        private static class HeaderViewHolder {
            ImageView icon;
            TextView title;
            TextView summary;
        }

        private final LayoutInflater mInflater;

        static int getHeaderType(Header header) {
            if (header.fragment == null && header.intent == null) {
                return HEADER_TYPE_CATEGORY;
            } else {
                return HEADER_TYPE_NORMAL;
            }
        }

        @Override
        public int getItemViewType(int position) {
            Header header = getItem(position);
            return getHeaderType(header);
        }

        @Override
        public boolean areAllItemsEnabled() {
            return false; // because of categories
        }

        @Override
        public boolean isEnabled(int position) {
            return getItemViewType(position) != HEADER_TYPE_CATEGORY;
        }

        @Override
        public int getViewTypeCount() {
            return HEADER_TYPE_COUNT;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        public HeaderAdapter(Context context, List<Header> objects) {
            super(context, 0, objects);
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            HeaderViewHolder holder;
            Header header = getItem(position);
            int headerType = getHeaderType(header);

            View view = null;
            if (convertView == null) {
                holder = new HeaderViewHolder();
                switch (headerType) {
                    case HEADER_TYPE_CATEGORY:
                        view = new TextView(getContext(), null, android.R.attr.listSeparatorTextViewStyle);
                        holder.title = (TextView) view;
                        break;
                    case HEADER_TYPE_NORMAL:
                        view = mInflater.inflate(R.layout.preference_header_item, parent, false);
                        holder.icon = (ImageView) view.findViewById(R.id.icon);
                        holder.title = (TextView) view.findViewById(R.id.title);
                        holder.summary = (TextView) view.findViewById(R.id.summary);
                        break;
                }
                assert view != null;
                view.setTag(holder);
            } else {
                view = convertView;
                holder = (HeaderViewHolder) view.getTag();
            }

            // All view fields must be updated every time, because the view may be recycled
            switch (headerType) {
                case HEADER_TYPE_CATEGORY:
                    holder.title.setText(header.getTitle(getContext().getResources()));
                    break;
                case HEADER_TYPE_NORMAL:
                    holder.icon.setImageResource(header.iconRes);
                    holder.title.setText(header.getTitle(getContext().getResources()));
                    CharSequence summary = header.getSummary(getContext().getResources());
                    if (!TextUtils.isEmpty(summary)) {
                        holder.summary.setVisibility(View.VISIBLE);
                        holder.summary.setText(summary);
                    } else {
                        holder.summary.setVisibility(View.GONE);
                    }
                    break;
            }
            return view;
        }
    }

}
