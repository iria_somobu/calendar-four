/*
 * Copyright (C) 2013-2016 Dominik Schürmann <dominik@dominikschuermann.de>
 * Copyright (C) 2012 Harald Seltner <h.seltner@gmx.at>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.somobu.calendar.preferences;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.larswerkman.holocolorpicker.ColorPicker;
import com.larswerkman.holocolorpicker.SVBar;

import com.somobu.calendar.R;

public class EditCalendarActivity extends Activity {

    boolean edit = false;

    private EditText displayNameEditText;
    ColorPicker colorPicker;

    long mCalendarId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_activity);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setDisplayShowHomeEnabled(true);

        displayNameEditText = (EditText) findViewById(R.id.edit_activity_text_cal_name);
        colorPicker = (ColorPicker) findViewById(R.id.edit_activity_color_picker);
        SVBar svBar = (SVBar) findViewById(R.id.edit_activity_svbar);

        colorPicker.addSVBar(svBar);

        // check if add new or edit existing
        Intent intent = getIntent();
        Uri calendarUri = intent.getData();
        if (calendarUri != null) {
            edit = true;
        }

        if (edit) {
            getActionBar().setTitle(R.string.edit_activity_name_edit);

            Cursor cur = getContentResolver().query(calendarUri, CalendarController.PROJECTION, null, null, null);
            mCalendarId = ContentUris.parseId(calendarUri);
            try {
                if (cur.moveToFirst()) {
                    String displayName = cur.getString(CalendarController.PROJECTION_DISPLAY_NAME_INDEX);
                    int color = cur.getInt(CalendarController.PROJECTION_COLOR_INDEX);

                    // display for editing
                    displayNameEditText.setText(displayName);
                    setColor(color);
                }
            } finally {
                if (cur != null && !cur.isClosed()) {
                    cur.close();
                }
            }
        } else {
            getActionBar().setTitle(R.string.edit_activity_name_new);

            setColor(getResources().getColor(R.color.emphasis));
            // on calendar creation, set both center colors to new color
            colorPicker.setOnColorChangedListener(new ColorPicker.OnColorChangedListener() {
                @Override
                public void onColorChanged(int color) {
                    colorPicker.setOldCenterColor(color);
                }
            });
        }

        // remove error when characters are entered
        displayNameEditText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                displayNameEditText.setError(null);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, "Save").setIcon(android.R.drawable.ic_menu_save).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        menu.add(1, 1, 1, "Delete");

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case 0:
                save();
                return true;
            case 1:
                delete();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void save() {
        if (displayNameEditText.getText().length() == 0) {
            displayNameEditText.setError(getString(R.string.edit_activity_error_empty_name));
        } else {
            displayNameEditText.setError(null);
            if (edit) {
                updateCalendar();
            } else {
                addCalendar(EditCalendarActivity.this);
            }
        }
    }

    private void delete() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.edit_activity_really_delete_title)
                .setMessage(R.string.edit_activity_really_delete)
                .setCancelable(false)
                .setPositiveButton(R.string.edit_activity_delete_dialog_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteCalendar();
                    }
                }).setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void setColor(int color) {
        colorPicker.setColor(color);
        colorPicker.setNewCenterColor(color);
        colorPicker.setOldCenterColor(color);
    }

    private void showMessageAndFinish(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message).setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void addCalendar(Context context) {
        try {
            CalendarController.addCalendar(context, displayNameEditText.getText().toString(), colorPicker.getColor(), getContentResolver());
            finish();
        } catch (IllegalArgumentException e) {
            showMessageAndFinish(getString(R.string.edit_activity_error_add));
        }
    }

    private void updateCalendar() {
        CalendarController.updateCalendar(mCalendarId, displayNameEditText.getText()
                .toString(), colorPicker.getColor(), getContentResolver());
        finish();
    }

    private void deleteCalendar() {
        if (CalendarController.deleteCalendar(mCalendarId, getContentResolver())) {
            finish();
        } else {
            showMessageAndFinish(getString(R.string.edit_activity_error_delete));
        }
    }

}
