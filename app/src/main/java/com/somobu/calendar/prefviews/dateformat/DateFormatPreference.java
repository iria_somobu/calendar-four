package com.somobu.calendar.prefviews.dateformat;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.preference.DialogPreference;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.somobu.calendar.R;

import org.joda.time.DateTime;
import org.somobu.todoagenda.prefs.AllSettings;
import org.somobu.todoagenda.prefs.ApplicationPreferences;
import org.somobu.todoagenda.prefs.InstanceSettings;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateFormatPreference extends DialogPreference
        implements AdapterView.OnItemSelectedListener, View.OnKeyListener, TextWatcher {

    DateFormatValue defaultValue = DateFormatType.unknownValue();
    DateFormatValue value = DateFormatType.unknownValue();

    LayoutInflater inflater;

    private Spinner typeSpinner;
    private EditText customPatternText;
    private EditText sampleDateText;
    private TextView resultText;

    private final DateFormatValue sampleDateFormatValue = DateFormatValue.of(DateFormatType.CUSTOM, "yyyy-MM-dd");


    public DateFormatPreference(Context context) {
        this(context, null);
    }

    public DateFormatPreference(Context context, AttributeSet attrs) {
        super(context, attrs);

        inflater = LayoutInflater.from(context);

        final Bundle b = new Bundle();
        b.putString("ARG_KEY", getKey());
//        setArguments(b);

        onSetInitialValue(null);
        showValue();
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        if (a.peekValue(index) != null && a.peekValue(index).type == TypedValue.TYPE_STRING) {
            return DateFormatValue.load(a.getString(index), DateFormatType.unknownValue());
        }
        return DateFormatType.unknownValue();
    }

    @Override
    public void setDefaultValue(Object defaultValue) {
        super.setDefaultValue(defaultValue);
        this.defaultValue = DateFormatValue.loadOrUnknown(defaultValue);
    }

    protected void onSetInitialValue(@Nullable Object defaultValue) {
        value = ApplicationPreferences.getDateFormat(getContext(), getKey(), this.defaultValue);
        showValue();
    }

    public DateFormatValue getValue() {
        return value.type == DateFormatType.UNKNOWN ? defaultValue : value;
    }

    @Override
    public CharSequence getSummary() {
        return value.getSummary(getContext());
    }

    public void setValue(DateFormatValue value) {
        this.value = value;
        ApplicationPreferences.setDateFormat(getContext(), getKey(), value);
        showValue();
    }

    private void showValue() {
        setSummary(getSummary());
    }

    @Override
    protected View onCreateDialogView() {
        LinearLayout dialogView = (LinearLayout) inflater.inflate(R.layout.dateformat_preference, null);
        Context context = inflater.getContext();

        typeSpinner = (Spinner) dialogView.findViewById(R.id.date_format_type);
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(
                context, android.R.layout.simple_spinner_item, DateFormatType.getSpinnerEntryList(context));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeSpinner.setAdapter(adapter);
        typeSpinner.setSelection(getValue().type.getSpinnerPosition());
        typeSpinner.setOnItemSelectedListener(this);

        customPatternText = (EditText) dialogView.findViewById(R.id.custom_pattern);
        customPatternText.setText(getValue().getPattern());
        customPatternText.addTextChangedListener(this);

        sampleDateText = (EditText) dialogView.findViewById(R.id.sample_date);
        sampleDateText.setText(getSampleDateText());
        sampleDateText.addTextChangedListener(this);

        resultText = (TextView) dialogView.findViewById(R.id.result);

        calcResult();

        return dialogView;
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);

        if (positiveResult) {
            DateFormatValue value = getValue().toSave();
            if (callChangeListener(value)) {
                setValue(value);
            }
        }
    }

    // Two methods to listen for the Spinner changes
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (getValue().type.hasPattern()) {
            customPatternText.setText(getValue().type.pattern);
        } else if (!getValue().hasPattern() && getValue().type.isCustomPattern()) {
            customPatternText.setText(DateFormatType.DEFAULT_EXAMPLE.pattern);
        }
        calcResult();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        calcResult();
    }

    // Four methods to listen to the Custom pattern text changes
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        calcResult();
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        return false;
    }


    private DateFormatValue getValue2() {
        int position = typeSpinner.getSelectedItemPosition();
        if (position >= 0) {
            DateFormatType selectedType = DateFormatType.values()[position];
            return DateFormatValue.of(selectedType, customPatternText.getText().toString());
        }
        return DateFormatType.UNKNOWN.defaultValue();
    }

    private void calcResult() {
        DateFormatValue dateFormatValue = getValue();
        SimpleDateFormat sampleFormat = getSampleDateFormat();
        CharSequence result;
        try {
            if (customPatternText.isEnabled() != dateFormatValue.type.isCustomPattern()) {
                customPatternText.setEnabled(dateFormatValue.type.isCustomPattern());
            }
            Date sampleDate = sampleFormat.parse(sampleDateText.getText().toString());
            result = sampleDate == null
                    ? "null"
                    : new DateFormatter(this.getContext(), dateFormatValue, getSettings().clock().now())
                    .formatDate(new DateTime(sampleDate.getTime(), getSettings().clock().getZone()));
        } catch (ParseException e) {
            result = e.getLocalizedMessage();
        }
        resultText.setText(result);
    }

    private CharSequence getSampleDateText() {
        return new DateFormatter(getContext(), sampleDateFormatValue, getSettings().clock().now())
                .formatDate(getSettings().clock().now());
    }

    private SimpleDateFormat getSampleDateFormat() {
        return new SimpleDateFormat(sampleDateFormatValue.getPattern(), Locale.ENGLISH);
    }

    private InstanceSettings getSettings() {
        int widgetId = ApplicationPreferences.getWidgetId(getContext());
        return AllSettings.instanceFromId(getContext(), widgetId);
    }
}