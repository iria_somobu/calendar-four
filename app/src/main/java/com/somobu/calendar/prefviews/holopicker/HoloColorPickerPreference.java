package com.somobu.calendar.prefviews.holopicker;

import android.content.Context;
import android.os.Handler;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.larswerkman.holocolorpicker.ColorPicker;
import com.larswerkman.holocolorpicker.OpacityBar;
import com.larswerkman.holocolorpicker.SVBar;
import com.somobu.calendar.R;

public class HoloColorPickerPreference extends DialogPreference {

    private ColorPicker picker = null;

    public HoloColorPickerPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HoloColorPickerPreference(Context context) {
        super(context);
    }

    @Override
    protected View onCreateDialogView() {
        int color = getSharedPreferences().getInt(getKey(), 0);

        View root = LayoutInflater.from(getContext()).inflate(R.layout.pref_colorpicker, null, false);

        picker = (ColorPicker) root.findViewById(R.id.picker);
        picker.setShowOldCenterColor(false);

        SVBar svBar = (SVBar) root.findViewById(R.id.svbar);
        picker.addSVBar(svBar);

        OpacityBar opacityBar = (OpacityBar) root.findViewById(R.id.opacitybar);
        picker.addOpacityBar(opacityBar);

        new Handler().post(() -> setColor(color));

        return root;
    }

    private void setColor(int color) {
        picker.setColor(color);
        picker.setNewCenterColor(color);
        picker.setOldCenterColor(color);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);

        if (positiveResult) {
            getSharedPreferences().edit().putInt(getKey(), picker.getColor()).apply();
        }
    }
}
