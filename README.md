# Calendar Four

Just a regular AOSP 4 calendar, but integrated with Offline Calendar and Todo Agenda.


## Download

You can download latest artifact from Gitlab CI via [this link](https://gitlab.com/iria_somobu/calendar-four/-/jobs/artifacts/master/file/calendar.apk?job=build).


## About
- Based on Standalone Calendar app;
- Distributed under GNU GPL v3;
- Contains several components under GNU GPL and Apache 2 licenses.
