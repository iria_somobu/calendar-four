# Request and check permissions

```java
// check Android 6 permission
if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR)
        == PackageManager.PERMISSION_GRANTED) {
    init();
} else {
    ActivityCompat.requestPermissions(this,
            new String[]{Manifest.permission.WRITE_CALENDAR, Manifest.permission.READ_CALENDAR},
            REQUEST_PERMISSIONS_WRITE_CALENDAR);
}
```


# Validate installed on internal storage

```java
/**
 * Offline Calendar must be install on internal location!
 *
 * from bug report (https://github.com/dschuermann/offline-calendar/issues/19):
 * I am using S2E, which extends phone disk space by putting apps to the SD card.
 * The SD card is mounted quite late during the boot process,
 * but Android needs sync adapters earlier at boot time to be able to use them.
 * As a result, sync adapters like the offline calendar seemed to disappear during boot,
 * although Android is simply not able to load it soon enough.
 */

if (InstallLocationHelper.isInstalledOnSdCard(this)) {
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setMessage(R.string.main_activity_sd_card_error).setCancelable(false)
            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    finish();
                }
            });
    AlertDialog alert = builder.create();
    alert.show();
}
```